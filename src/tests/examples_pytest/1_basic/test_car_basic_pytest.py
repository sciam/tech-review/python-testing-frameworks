import pytest

from src.car import Car


def test_car_add_fuel():
    car = Car(initial_fuel=0)
    car.add_fuel(80)
    assert car.fuel == 80


if __name__ == '__main__':
    pytest.main()
