import pytest
from src.car import Car, InsufficientFuelException


@pytest.fixture
def car() -> Car:
    """Return a car instance with 20L of fuel in it"""
    print("\nSetting up...")
    car = Car(initial_fuel=20)
    return car

    # Also possible instead of return
    # yield car
    # print("\nTearing down...")
    # del car


def test_car_drive(car: Car):
    car.drive(18.01)
    assert car.fuel == pytest.approx(2.0, 0.01)


def test_car_add_fuel(car: Car):
    car.add_fuel(80)
    assert car.fuel == 100


def test_car_drive_with_not_enough_fuel(car: Car):
    with pytest.raises(InsufficientFuelException):
        car.drive(100)


if __name__ == "__main__":
    pytest.main()
