import pytest
from src.car import Car, InsufficientFuelException


@pytest.mark.xfail(raises=InsufficientFuelException)
def test_car_drive_with_not_enough_fuel(global_car: Car):
    """
    This test will be marked as "expected to fail" instead of as "passed". Xfail decorator is more
    used to prevent a test failing with a known bug than testing if a function will fail. In the
    latter, use pytest.raises.
    """
    global_car.drive(100)
