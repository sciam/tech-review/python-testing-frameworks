import pytest
from src.car import Car


@pytest.mark.parametrize("fuel,distance,expected", [(30, 30, 20), (0, 18, 2)])
def test_drives(fuel: int, distance: int, expected: int):
    car = Car(initial_fuel=20)
    car.add_fuel(fuel)
    car.drive(distance)
    assert car.fuel == expected
