from pytest_mock.plugin import MockerFixture
from src.car import Car


def test_handle_headlights_when_bright(mocker: MockerFixture):
    # get_brightness is from light_sensor.py but imported to car.py
    mocker.patch("src.car.get_brightness", return_value="bright")
    car = Car()
    car.handle_headlights()
    assert car.use_headlights is False


def test_handle_headlights_when_dark(mocker: MockerFixture):
    # get_brightness is from light_sensor.py but imported to car.py
    mocked_get_brightness = mocker.patch("src.car.get_brightness", return_value="dark")
    car = Car()
    car.handle_headlights()
    assert car.use_headlights is True
    assert mocked_get_brightness.called is True
    assert mocked_get_brightness.call_count == 1
