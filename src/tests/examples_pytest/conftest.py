"""
Pytest will automatically check all conftest.py files in the test folder and sub-folders,
and import all fixtures from there. You can also pass other configuration scripts in there.
It will also automatically modify the path from which pytest is run.
"""
import pytest

from src.car import Car


@pytest.fixture
def global_car():
    """
    Return a car instance with 20L of fuel in it. This fixture can be used in any tests and
    will be discovered by Pytest.
    """
    print("\nSetting up...")
    car = Car(initial_fuel=20)

    yield car

    print("\nTearing down...")
    del car
