# Run pytest tests from CLI

> py.test

In this file, this does not work since there are multiple test files named the same way + 
conftest is not at the right place, which makes importing fail. However, if you remove all 
other files than pytest + move your tests in the right place + put conftest in the src package, 
the py.test command should work.