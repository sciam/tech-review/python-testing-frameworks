import unittest

from parameterized import parameterized

from src.car import Car

param_list = [(30, 20, 30), (0, 18, 2)]


class ParameterizedCarTestCase(unittest.TestCase):
    def setUp(self) -> None:
        print("\nSetting up...")
        self.car = Car(initial_fuel=20)

    def tearDown(self) -> None:
        print("\nTearing down...")
        del self.car

    @parameterized.expand(param_list)
    def test_car_drives(self, fuel, distance, expected):
        self.car.add_fuel(fuel)
        self.car.drive(distance)
        self.assertEqual(self.car.fuel, expected)

    def test_car_drives_will_fail(self):  # Fails! Set up / Tear Down not done at each sub test.
        for i, (fuel, distance, expected) in enumerate(param_list):
            with self.subTest(msg=i):  # since python 3.4
                self.car.add_fuel(fuel)
                self.car.drive(distance)
                self.assertEqual(self.car.fuel, expected)


if __name__ == '__main__':
    unittest.main()
