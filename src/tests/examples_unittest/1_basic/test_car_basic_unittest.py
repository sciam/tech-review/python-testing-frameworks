import unittest

from src.car import Car


class BasicCarTestCase(unittest.TestCase):
    def test_car_add_fuel(self):
        car = Car(initial_fuel=20)  # test me with consumption == 0 !
        car.add_fuel(20)
        self.assertEqual(40, car.fuel)


if __name__ == '__main__':
    unittest.main()
