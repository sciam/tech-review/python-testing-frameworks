# Discover and run all tests in the project

> python -m unittest discover

# Run all tests in the given module

> python -m unittest unittest-tests

# Run all tests in the given test class

> python -m unittest unittest.test_car_basic.BasicCarTestCase

# Run all tests in the given Python file

> python -m unittest unittest-tests/test_car_basic.py