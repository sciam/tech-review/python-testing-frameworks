import unittest

from src.car import Car, InsufficientFuelException


class BasicCarTestCase(unittest.TestCase):
    def setUp(self) -> None:
        print("\nSetting up...")
        self.car = Car(initial_fuel=20)

    def tearDown(self) -> None:
        print("\nTearing down...")
        del self.car

    def test_car_add_fuel(self):
        self.car.add_fuel(20)
        self.assertEqual(self.car.fuel, 40)

    def test_car_drive(self):
        self.car.drive(18.0)
        self.assertAlmostEqual(self.car.fuel, 2.001, places=1)

    def test_car_drive_not_enough_fuel(self):
        self.assertRaises(InsufficientFuelException, self.car.drive, 10000)


if __name__ == '__main__':
    unittest.main()
