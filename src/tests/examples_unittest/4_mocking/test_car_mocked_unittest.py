import unittest
from unittest.mock import patch

from src.car import Car


class CarHandleHeadlightsTestCase(unittest.TestCase):
    def test_handle_headlights_when_bright(self):
        # get_brightness is from light_sensor.py but imported to car.py
        with patch('src.car.get_brightness') as mock_get_brightness:
            mock_get_brightness.return_value = "bright"
            car = Car()
            car.handle_headlights()
            self.assertFalse(car.use_headlights)

    # get_brightness is from light_sensor.py but imported to car.py
    @patch('src.car.get_brightness', return_value="dark")
    def test_handle_headlights_when_dark(self, mock_get_brightness):
        car = Car()
        car.handle_headlights()
        self.assertTrue(car.use_headlights)
        self.assertTrue(mock_get_brightness.called)
        self.assertEqual(1, mock_get_brightness.call_count)
