import nose
from mockito import when

import src
from src.car import Car


def test_handle_headlights_when_bright():
    # get_brightness is from light_sensor.py but imported to car.py
    when(src.car).get_brightness().thenReturn("bright")
    car = Car()
    car.handle_headlights()
    assert car.use_headlights is False


def test_handle_headlights_when_dark():
    # get_brightness is from light_sensor.py but imported to car.py
    when(src.car).get_brightness().thenReturn("dark")
    car = Car()
    car.handle_headlights()
    assert car.use_headlights is True


if __name__ == '__main__':
    nose.run()  # can use nose.runmodule() for tests in module if not in IDE
