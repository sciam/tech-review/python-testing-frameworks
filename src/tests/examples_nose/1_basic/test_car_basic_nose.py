import nose

from src.car import Car


def test_car_add_fuel():
    car = Car(20)
    car.add_fuel(80)
    assert car.fuel == 100


if __name__ == '__main__':
    nose.run()  # can use nose.runmodule() for tests in module if not in IDE
