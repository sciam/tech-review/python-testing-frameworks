import nose
from parameterized import parameterized  # also possible to use "from nose_parameterized ..."

from src.car import Car


@parameterized([(30, 30, 20), (0, 18, 2)])
def test_drives(fuel, distance, expected):
    car = Car(20)
    car.add_fuel(fuel)
    car.drive(distance)
    assert car.fuel == expected


if __name__ == '__main__':
    nose.run()  # can use nose.runmodule() for tests in module if not in IDE
