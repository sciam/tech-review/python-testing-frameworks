import nose
from nose import with_setup
from nose.tools import raises

from src.car import Car, InsufficientFuelException


def setup():
    print("\nSetting up...")


def teardown():
    print("\nTearing down...")


@with_setup(setup=setup, teardown=teardown)
def test_car_add_fuel():
    car = Car(20)
    car.add_fuel(80)
    assert car.fuel == 100


@with_setup(setup=setup, teardown=teardown)
def test_car_drive():
    car = Car(20)
    car.drive(10)
    assert car.fuel == 10


@with_setup(setup=setup, teardown=teardown)
@raises(InsufficientFuelException)
def test_car_drive_with_not_enough_fuel():
    car = Car(20)
    car.drive(100)


if __name__ == '__main__':
    nose.run()  # can use nose.runmodule() for tests in module if not in IDE
