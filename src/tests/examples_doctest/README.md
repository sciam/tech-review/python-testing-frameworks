# Run doctests embedded as docstrings

> python -m doctest src/car.py

# Run doctests written in separate text files

> python -m doctest src/tests/examples-doctest/test_car.txt
