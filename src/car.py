from src.light_sensor import get_brightness


class Car:
    """
    Example class to be tested by multiple Python unit testing frameworks.
    """

    def __init__(self, initial_fuel: float = 0.0, consumption: float = 1.0):
        """
        :param initial_fuel: Initial quantity of fuel in the tank (in L)
        :param consumption: consumption per km. Default 1 L/km
        """
        self.fuel = initial_fuel
        self.consumption = consumption
        self.use_headlights = False

    def drive(self, distance: float):
        """
        Drives a specific distance, in kms.

        >>> car = Car(initial_fuel=10)
        >>> car.drive(5)
        >>> car.fuel
        5.0

        >>> car.drive(200)
        Traceback (most recent call last):
            ...
        car.InsufficientFuelException: Not enough fuel available to drive 200.

        :param distance: distance in kms
        """
        fuel_needed = distance / self.consumption
        if self.fuel < fuel_needed:
            raise InsufficientFuelException(f"Not enough fuel available to drive {distance}.")

        self.fuel -= fuel_needed

    def add_fuel(self, amount: float):
        """
        Refuels the car's tank.

        >>> car = Car()
        >>> car.add_fuel(10)
        >>> car.fuel
        10.0

        :param amount: amount of fuel in L
        """
        self.fuel += amount

    def handle_headlights(self):
        """
        Checks the external brightness and activates or deactivates the headlights accordingly.
        """
        brightness = get_brightness()
        if brightness == "dark":
            self.use_headlights = True
        elif brightness == "bright":
            self.use_headlights = False


class InsufficientFuelException(Exception):
    pass
