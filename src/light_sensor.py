import time


def get_brightness() -> str:
    """
    Gets the brightness in the surrounding environment. This function is there to simulate the
    fact we are calling an external system.
    :return: "dark" or "bright"
    """
    print("Long process...")
    time.sleep(2)
    return "dark"
